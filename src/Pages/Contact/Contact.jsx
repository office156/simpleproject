import React from 'react'
import "./Contact.css"
import Navbar from '../../Components/Navbar/Navbar';
import Footer from '../../Components/Footer/Footer';

export default function ContactUs() {
  return (
    <div><Navbar></Navbar>
    <div class="contactcontainer">
        <h2 style={{textAlign:"center"}}>Contact us</h2>
		<p style={{textAlign:"center"}}>If you have any questions or comments, please feel free to contact us:</p>
		<address>
			C-DAC, Innovation Park
			HPC-TECH Dept, 4th Floor (South Side)<br></br>
			Panchwati, Pashan<br></br>
			Pune - 411008<br></br>
			Email : <a href="mailto:nsm-training@cdac.in">nsm-training@cdac.in</a>
        </address>
	</div>

<Footer></Footer>

</div>

)}
