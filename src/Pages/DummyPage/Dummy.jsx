import React from 'react'
import "./Dummy.css"
import "animate.css"

export default function Dummy() {
  return (
    <div>
        <div className="dummypagetop w-100">
            <h1 className='animate__animated animate__flip'>This is a dummy page !</h1>
            {/* animate__flip */}
        </div>
    </div>
  )
}
