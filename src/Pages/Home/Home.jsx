import React from 'react';
import ImageSlider from '../../Components/ImageSlider/ImageSlider';
import Navbar from '../../Components/Navbar/Navbar';
import Footer from '../../Components/Footer/Footer';
import { useNavigate } from 'react-router-dom';
import "./Home.css"


export default function Home() {

  const navigate = useNavigate();

  return <div className='HomePageTop'>
      <Navbar></Navbar>
      <ImageSlider></ImageSlider>

      <button className='dummylinkbutton m-5 button-30 ' onClick={(e)=>{
            e.preventDefault();
            navigate('/dummy');
          }}>Let's Start</button>
      <br />
      <br />

      <Footer></Footer>
  </div>;
}
