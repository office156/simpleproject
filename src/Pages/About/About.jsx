import React from 'react'
import "./About.css"
import Navbar from '../../Components/Navbar/Navbar';
import Footer from '../../Components/Footer/Footer';
import DG from '../../Images/DG.jpg'
import ED from '../../Images/ED.jpg'
import NSMMISSION from '../../Images/NSMMISSION.jpg'
import NSMHRD from '../../Images/NSMHRD.jpg'

export default function AboutUs() {

  return (
    <div>
      <Navbar></Navbar>
    <div className='AboutUsTopContainer'>
      <div class="container MyAboutus">

        <h1>About Us</h1>
        <h2>Our Individuality</h2>
        <p>CDAC (Centre for Development of Advanced Computing) is a research and development organization that focuses on advanced computing technologies, while NSM HRD (National Supercomputing Mission Human Resource Development) is a government initiative that aims to improve the employability of the Indian workforce through skill development programs.</p>
        <h2>Our Profession</h2>
        <p>CDAC conducts research and development in various areas such as high-performance computing, cybersecurity, artificial intelligence, and natural language processing. The organization also offers training and certification programs to enhance the skills of professionals in the IT industry.
          NSM HRD, on the other hand, aims to provide skill training to millions of unemployed youth in India. The initiative focuses on enhancing the quality and relevance of skill training programs to make them more effective in meeting the demands for the niche areas like HPC, DL/ML, AI and Quantum Computing.</p>
        <h2>Our Expertise</h2>
        <p>Expertise in High Performance Computing (HPC), Artificial Intelligence (AI), and Deep Learning/Machine Learning (DL/ML) requires a combination of technical knowledge, practical experience, and a deep understanding of the underlying mathematical and statistical principles. In HPC, expertise involves knowledge of parallel programming, optimization techniques, hardware architectures, and software libraries to efficiently execute large-scale simulations and computations. In AI, expertise involves knowledge of various algorithms, techniques, and tools used for solving complex problems using machine learning and artificial intelligence. This includes supervised and unsupervised learning, neural networks, decision trees, and natural language processing. In DL/ML, expertise involves knowledge of advanced neural network architectures, deep learning frameworks, and statistical methods for training models.</p>
      </div>


      <section class="team-section">
        <h2>Our Team</h2>
        <p>As a team, we believe in collaboration and open communication. Our team is made up of individuals with diverse backgrounds and skill sets. This diversity is what makes us strong, and we leverage each other's strengths to achieve our goals</p>


        <div class="card">
        <img src={DG} alt="Team Member 1"></img>
          <div class="card-content">
            <h4>Shri. E Magesh, DG</h4>
            <p className='d-none d-md-block'>More than 29 years of diversified experience ranging from Design Engineering, Project Management and delivery of turnkey projects. He has in depth expertise in Electronics and CHIP design, VLSI, Hardware, IP, Complete Product Cycle.His visionary approach in conceptualizing ideas in Quantum Technologies is helping to establish a Quantum Computer.</p>
          </div>
        </div>

        <div class="card">
        <img src={ED} alt="Team Member 2"></img>
          <div class="card-content">
            <h4>Col. A.K. Nath, ED</h4>
            <p className='d-none d-md-block'>Commissioned in June 1985 into Corps of Signals, Indian Army, an aluminous of National Defence Academy, Khadakwasla and India Military Academy, Dehradun. He did his B.Tech from Military College of Telecom Engineering, Mhow, MP and M.Tech from Indian Institute of Technology, Kanpur (IIT Kanpur)</p>
          </div>
        </div>

        <div class="card">
        <img src={NSMMISSION} alt="Team Member 3"></img>
          <div class="card-content">
            <h4>Sanjay Whandekar, NSM Mission Head</h4>
            <p className='d-none d-md-block'>Master of Technology in Digital Systems, experienced with over 25 years of proven track record in all phases of product design and development from the conceptual stages to high volume production release and deployment in the field. Currently coordinating the National Supercomputing Mission activities at CDAC and leading the activity of design, development and deployment of supercomputers.</p>
          </div>
        </div>

        <div class="card">
        <img src={NSMHRD} alt="Team Member 4"></img>
          <div class="card-content">
            <h4>Ashish Kuvelkar, NSM HRD Head</h4>
            <p className='d-none d-md-block'>Master’s Degree in Electrical Engineering, involved in development of hardware subsystems for various generations of PARAM supercomputing systems. Currently, as a Convener of NSM Expert Group on HR Development, involved in coordinating the activities of the Expert Group and implementing its mandate by working with various Stakeholders from educational institutes and the industry.</p>
          </div>
        </div>



      </section>



      

    </div>
    
    <Footer></Footer>
    </div>

  )
}



