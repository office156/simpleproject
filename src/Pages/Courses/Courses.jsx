import React from 'react'
import Cards from '../../Components/Cards/Cards'
import Navbar from '../../Components/Navbar/Navbar'
import Footer from '../../Components/Footer/Footer'
import "./Courses.css"


export default function Courses() {
  return (
    <div>

        <Navbar></Navbar>

    <div className='container coursespagetop'>
        <h1 className="text-center mb-5">Courses</h1><br/>

        <div className='row coursepagerow'>
            <div className="col">
            <Cards title="HPC" ></Cards>
            </div>
            <div className="col">
            <Cards title="MPI" ></Cards>
            </div>
            <div className="col">
            <Cards title="CUDA" ></Cards>
            </div>
            <div className="col">
            <Cards title="OpenMP" ></Cards>
            </div>

        </div>
    </div>

    <Footer></Footer>

    </div>
  )
}
