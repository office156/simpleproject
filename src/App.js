import AboutUs from "./Pages/About/About";
import ContactUs from "./Pages/Contact/Contact";
import Courses from "./Pages/Courses/Courses";
import Dummy from "./Pages/DummyPage/Dummy";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Login/Login";
import { BrowserRouter as Router, Switch, Route, Routes } from 'react-router-dom';


function App() {
  return (

    // <AuthContext.Provider value={{ loggedIn, setLoggedIn }}>
    <Router>
    <div className="App">
      <Routes>
      <Route exact path='/' element={< Home />}></Route>
      <Route exact path='/home' element={< Home />}></Route>
                 <Route exact path='/about' element={< AboutUs />}></Route>
                 <Route exact path='/contact' element={< ContactUs />}></Route>
                 <Route exact path="/login" element={<Login />}/>
                 <Route exact path="/dummy" element={<Dummy />}/>
                 <Route exact path="/courses" element={<Courses />}/>
          </Routes>
    </div>
    </Router>
    // {/* </AuthContext.Provider> */}
    
  );
}

export default App;
