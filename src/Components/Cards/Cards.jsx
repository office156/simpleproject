import React from 'react'
import cardimage from "../../Images/bg1.jpg"
import "./Cards.css"

export default function Cards({title}) {
  return (
    <div className='mb-4'>
        <div class="card" >
  <img src={cardimage} class="card-img-top cardimage" alt="..."></img>
  <div class="card-body">
    <h5 class="card-title">{title}</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>

{/* style="width: 18rem;" */}
    </div>
  )
}
