import React from 'react';
import "./Navbar.css"
import { Link ,useNavigate  } from 'react-router-dom';
import cdaclogo from '../../Images/cdaclogo.png'
import { useContext } from 'react';
// import { AuthContext } from '../../App';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.

export default function Navbar() {

  const navigate = useNavigate();
  // const { loggedIn } = useContext(AuthContext);

  function handleClick() { 
    navigate('/registration');
  }

  return <div>
 {/* <!-- Navigation --> */}

<nav class="navbar navbar-expand-lg bg-light">
  <div class="container">
    <a class="navbar-brand NavbarBrand" onClick={(e)=>{
            e.preventDefault();
            navigate('/home');
          }} >
            <img src={cdaclogo} className='cdaclogo' alt="" />
            HPCTraining</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ms-auto">
        <li class="nav-item learningPathLink">
          <a class="nav-link" aria-current="page" onClick={(e)=>{
            e.preventDefault();
            navigate('/home');
          }}>Home</a>
        </li>
        <li class="nav-item learningPathLink">
          <a class="nav-link" aria-current="page" onClick={(e)=>{
            e.preventDefault();
            navigate('/about');
          }}>About Us</a>
        </li>
        <li class="nav-item learningPathLink">
          <a class="nav-link" aria-current="page" onClick={(e)=>{
            e.preventDefault();
            navigate('/contact');
          }}>Contact Us</a>
        </li>
        <li class="nav-item learningPathLink">
          <a onClick={(e)=>{
            e.preventDefault();
            navigate('/courses');
          }} class="nav-link" >Courses</a>
        </li>

        <li class="nav-item learningPathLink">
          <a onClick={(e)=>{
            e.preventDefault();
            navigate('/login');
          }} class="nav-link">Login</a>
        </li>



    </ul>
    </div>
  </div>
</nav>

  </div>;
}
